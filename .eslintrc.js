module.exports = {
  root: true,
  parser: 'babel-eslint',
  extends: [
    '@react-native-community',
    'airbnb',
    'plugin:prettier/recommended',
    'prettier/react',
    'plugin:import/recommended',
    'plugin:jsx-a11y/recommended',
    'plugin:react/recommended',
    'plugin:react-native/all',
    'plugin:jest/recommended'
  ],
  parserOptions: {
    ecmaFeatures: {
      jsx: true,
      modules: true
    }
  },
  settings: {
    'import/resolver': {
      alias: {
        map: [
          ['@app', './app'],
          ['@components', './app/components'],
          ['@images', './app/configs/images'],
          ['@svgs', './assets/svgs'],
          ['@styles', './app/styles'],
          ['@helper', './app/utils'],
          ['@i18n', './app/i18n'],
          ['@data', './app/data'],
          ['@domain', './app/domain'],
          ['@mapper', './app/mapper'],
          ['@constants', './app/constants'],
          ['@routes', './app/routes']
        ],
        extensions: ['.js', '.jsx']
      }
    }
  },
  rules: {
    'class-methods-use-this': 0,
    'linebreak-style': 0,
    'no-underscore-dangle': 0,
    'object-curly-newline': 0,
    'react/prop-types': 1,
    'react-hooks/rules-of-hooks': 'error',
    'react-hooks/exhaustive-deps': 'off',
    'react/display-name': 0,
    'max-lines': ['error', 300],
    'max-len': ['error', {code: 100}],
    'max-depth': ['error', 4],
    'max-params': ['error', 4],
    'react/prefer-stateless-function': 0,
    'react/no-array-index-key': 0,
    'react-native/no-inline-styles': 0,
    'react-native/split-platform-components': 0,
    'react-native/sort-styles': 0,
    'react/jsx-props-no-spreading': 'off',
    'react/jsx-filename-extension': [
      1,
      {
        extensions: ['.js', '.jsx']
      }
    ],
    'import/no-extraneous-dependencies': [
      'error',
      {
        devDependencies: true
      }
    ],
    'prettier/prettier': [
      'error',
      {
        trailingComma: 'none',
        endOfLine: 'auto',
        singleQuote: true,
        printWidth: 100
      }
    ]
  },
  plugins: ['jest', 'prettier', 'import', 'jsx-a11y', 'react', 'react-native']
};
