# React Native Codebase

This repo contain project folder of React Native codebase

## Features
- Using 0.64.2 version
- Included essential dependency packages 

## Installation

```sh
yarn install
yarn start
yarn android
```

Regards, Bagus Subagja.

