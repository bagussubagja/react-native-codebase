module.exports = {
  presets: ['module:metro-react-native-babel-preset', 'module:react-native-dotenv'],
  plugins: [
    [
      'module-resolver',
      {
        alias: {
          '@app': './app',
          '@components': './app/components',
          '@images': './app/configs/images',
          '@svgs': './assets/svgs',
          '@styles': './app/styles',
          '@helper': './app/utils',
          '@i18n': './app/i18n',
          '@data': './app/data',
          '@domain': './app/domain',
          '@mapper': './app/mapper',
          '@constants': './app/constants',
          '@routes': './app/routes'
        }
      }
    ]
  ]
};
