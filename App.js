/* eslint-disable no-console */
import React, {Component} from 'react';
import Router from './app/routers';

export default class App extends Component {
  render() {
    console.disableYellowBox = true;
    const originalConsoleError = console.error;
    console.error = message => {
      if (message.startsWith('Warning: Encountered two children with the same key')) {
        return;
      }
      originalConsoleError(message);
    };
    return <Router />;
  }
}
