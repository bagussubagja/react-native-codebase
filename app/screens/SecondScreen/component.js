/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import {Button, Text, View} from 'react-native';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View>
        <Text>Pergi ke screen 3 dengan params</Text>
        <Button
          title="Pindah"
          onPress={() =>
            this.props.navigation.navigate('ThirdScreen', {paramsData: 'Bagus Subagja'})
          }
        />
      </View>
    );
  }
}
