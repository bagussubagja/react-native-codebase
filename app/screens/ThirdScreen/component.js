/* eslint-disable no-useless-catch */
/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import _ from 'lodash';
import React from 'react';
import {Text, View} from 'react-native';
import {ENDPOINT} from '../../configs';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      dataParams: '',
      data: []
    };
  }

  async componentDidMount() {
    await this.getParams();
    await this._getData();
  }

  getParams = async () => {
    const dataParams = _.get(this.props.navigation.state, 'params.paramsData', '');
    this.setState({
      dataParams
    });
  };

  _getData = async () => {
    try {
      const result = await ENDPOINT.getPostDataSample();
      if (result.length > 0) {
        await this.setState({
          data: result
        });
      }
    } catch (error) {
      throw error;
    }
  };

  render() {
    return (
      <View>
        <Text>{this.state.dataParams}</Text>
        {this.state.data.map((data, index) => (
          <Text key={index}>{data.title}</Text>
        ))}
      </View>
    );
  }
}
