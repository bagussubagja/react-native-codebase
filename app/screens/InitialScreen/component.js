/* eslint-disable react/prop-types */
/* eslint-disable react/destructuring-assignment */
import React from 'react';
import {Button, Image, Text, View} from 'react-native';
import styles from './styles';
import images from '../../configs/images';

export default class Component extends React.Component {
  constructor(props) {
    super(props);
    this.state = {};
  }

  render() {
    return (
      <View style={styles.container}>
        <Image source={images.placeholder} style={styles.imageStyle} />
        <Text style={styles.textStyle}>Hello World</Text>
        <Button title="Click Me" onPress={() => this.props.navigation.navigate('Second')} />
      </View>
    );
  }
}
