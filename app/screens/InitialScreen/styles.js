import {StyleSheet} from 'react-native';
import {FONT_PRIMARY_REGULAR} from '../../styles';
import {moderateScale} from '../../utils/scaling';

const styles = StyleSheet.create({
  container: {
    flex: 1,
    justifyContent: 'center',
    alignItems: 'center'
  },
  imageStyle: {
    width: 200,
    height: 200,
    resizeMode: 'stretch'
  },
  textStyle: {
    fontFamily: FONT_PRIMARY_REGULAR,
    fontSize: moderateScale(14)
  }
});

export default styles;
