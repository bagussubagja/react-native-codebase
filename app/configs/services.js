/* eslint-disable no-unused-vars */
import {get, post, put, remove} from './networking';

export const endpoint = {
  getPostDataSample: async () => get(`/posts`)
};

export default {endpoint};
