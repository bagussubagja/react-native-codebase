import {StackNavigator} from 'react-navigation';
import InitialScreen from '../screens/InitialScreen';
import SecondScreen from '../screens/SecondScreen';
import ThirdScreen from '../screens/ThirdScreen';

export const ScreenStack = StackNavigator({
  InitialScreen: {
    screen: InitialScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null
    }
  }
});

export const SecondStack = StackNavigator({
  SecondScreen: {
    screen: SecondScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null
    }
  },
  ThirdScreen: {
    screen: ThirdScreen,
    navigationOptions: {
      tabBarVisible: false,
      header: null
    }
  }
});
