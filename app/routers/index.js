import React from 'react';
import {SwitchNavigator} from 'react-navigation';
import {View} from 'react-native';
import {ScreenStack, SecondStack} from './stackNavigator';
import NavigationService from '../providers/navRef';

const AppNavigator = SwitchNavigator(
  {
    Initial: ScreenStack,
    Second: SecondStack
  },
  {initialRouteName: 'Initial'}
);

const App = () => {
  return (
    <View style={{flex: 1}}>
      <AppNavigator
        ref={navigatorRef => {
          NavigationService.setTopLevelNavigator(navigatorRef);
        }}
      />
    </View>
  );
};
export default App;
