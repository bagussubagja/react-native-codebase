/* eslint-disable import/prefer-default-export */
export const COLOR_WHITE = '#FFFFFF';
export const FONT_PRIMARY_REGULAR = 'Montserrat-Regular';
export const FONT_PRIMARY_MEDIUM = 'Montserrat-Medium';
export const FONT_PRIMARY_BOLD = 'Montserrat-SemiBold';
export const FONT_PRIMARY_ITALIC = 'Montserrat-Italic';
